import React from "react";
import "./App.css";
import Home from "./components/Home/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import UserLogin from "./components/Login/UserLogin";
import ViewArticle from "./components/Article/ViewArticle";
import AddArticle from "./components/Article/AddArticle";
import EditArticle from "./components/Article/EditArticle";
import UserRegister from "./components/Login/UserRegister";
import User from "./components/User/User";
import Category from "./components/Category/Category";

function App() {
  return (
    <>
      <Router>
        <div>
          <Switch>
            <Route exact path="/" component={UserLogin} />
            <Route path="/home" component={Home} />
            <Route path="/view_article/:id" component={ViewArticle} />
            <Route path="/add_article" component={AddArticle} />
            <Route path="/edit_article/:id" component={EditArticle} />
            <Route path="/register" component={UserRegister} />
            <Route path="/user" component={User} />
            <Route path="/category" component={Category} />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
