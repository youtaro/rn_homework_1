import { baseUrl } from "../../../config/baseUrl";
import axios from "axios";

export const GET_USER = "GET_USER";
export const ADD_USER = "ADD_USER";
export const GET_ALL_USER = "GET_ALL_USER";
export const DELETE_USER = "DELETE_USER";

export const getUser = (email) => {
  return async (dp) => {
    const result = await axios.get(`${baseUrl}/users/email/${email}`);
    dp({
      type: GET_USER,
      user: result.data,
    });
  };
};

export const getAllUser = () => {
  return async (dp) => {
    const result = await axios.get(`${baseUrl}/users/`);
    dp({
      type: GET_ALL_USER,
      allUser: result.data,
    });
  };
};

export const addUser = (user, cb) => {
  axios
    .post(`${baseUrl}/users/add`, user)
    .then((res) => {
      cb(res);
    })
    .catch(function (error) {
      cb({
        type: ADD_USER,
        payLoad: false,
      });
    });
};

export const updateUser = (id, user, cb) => {
  axios.put(`${baseUrl}/users/update/${id}`, user).then((res) => {
    cb(res);
  });
};

export const deleteUser = (id) => {
  return (dp) => {
    axios.delete(`${baseUrl}/users/delete/${id}`).then((res) => {
      dp({
        type: DELETE_USER,
        payLoad: id,
      });
    });
  };
};
