import { baseUrl } from "../../../config/baseUrl";
import axios from "axios";

export const GET_ARTICLE = "GET_ARTICLE";
export const VIEW_ARTICLE = "VIEW_ARTICLE";
export const GET_ARTICLE_CATEGORY = "GET_ARTICLE_CATEGORY";
export const POST_ARTICLE = "POST_ARTICLE";
export const DELETE_ARTICLE = "DELETE_ARTICLE";

export const getArticle = () => {
  return async (dp) => {
    const result = await axios.get(`${baseUrl}/articles/`);
    dp({
      type: GET_ARTICLE,
      article: result.data,
    });
  };
};

export const getArticleByCat = (cId) => {
  return async (dp) => {
    const result = await axios.get(`${baseUrl}/articles/?categoryId${cId}`);
    dp({
      type: GET_ARTICLE_CATEGORY,
      articleCat: result.data,
    });
  };
};

export const viewArticle = (id) => {
  return async (dp) => {
    const result = await axios.get(`${baseUrl}/articles/${id}`);
    dp({
      type: VIEW_ARTICLE,
      viewArt: result.data,
    });
  };
};

export const addArticle = (article, cb) => {
  axios
    .post(`${baseUrl}/articles/`, article)
    .then((res) => {
      cb(res);
    })
    .catch(function (error) {
      cb({
        type: POST_ARTICLE,
        payLoad: false,
      });
    });
};

export const deleteArticle = (id) => {
  return (dp) => {
    axios.delete(`${baseUrl}/articles/${id}`).then((res) => {
      dp({
        type: DELETE_ARTICLE,
        payLoad: id,
      });
    });
  };
};

export const editArticle = (id, cb) => {
  axios.get(`${baseUrl}/articles/${id}`).then((res) => {
    cb(res.data);
  });
};

export const updateArticle = (id, article, cb) => {
  axios.put(`${baseUrl}/articles/${id}`, article).then((res) => {
    cb(res);
  });
};
