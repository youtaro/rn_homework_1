import { baseUrl } from "../../../config/baseUrl";
import axios from "axios";

export const GET_CATEGORY = "GET_CATEGORY";
export const POST_CATEGORY = "POST_CATEGORY";
export const DELETE_CATEGORY = "DELETE_CATEGORY";

export const getCategory = () => {
  return async (dp) => {
    const result = await axios.get(`${baseUrl}/categories/all`);
    dp({
      type: GET_CATEGORY,
      category: result.data,
    });
  };
};

export const addACategory = (category, cb) => {
  axios
    .post(`${baseUrl}/categories/add/`, category)
    .then((res) => {
      cb(res);
    })
    .catch(function (error) {
      cb({
        type: POST_CATEGORY,
        payLoad: false,
      });
    });
};

export const updateCategory = (id, category, cb) => {
  axios.put(`${baseUrl}/categories/update/${id}`, category).then((res) => {
    cb(res);
  });
};

export const deleteCategory = (id) => {
  return (dp) => {
    axios.delete(`${baseUrl}/categories/delete/${id}`).then((res) => {
      dp({
        type: DELETE_CATEGORY,
        payLoad: id,
      });
    });
  };
};
