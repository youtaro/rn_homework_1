import { combineReducers } from "redux";
import { articleReducers } from "./Article/articleReducers";
import { categoryReducers } from "./Category/categoryReducers";
import { loginReducers } from "./Login/loginReducers";
const reducers = {
  loginReducers: loginReducers,
  articleReducers: articleReducers,
  categoryReducers: categoryReducers,
};
export const rootReducer = combineReducers(reducers);
