import {
  GET_ARTICLE,
  GET_ARTICLE_CATEGORY,
  VIEW_ARTICLE,
  DELETE_ARTICLE,
} from "../../actions/Article/articleActions";

const initialState = {
  article: [],
  articleCat: [],
  viewArt: [],
};

export const articleReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_ARTICLE:
      return {
        ...state,
        article: action.article,
      };

    case GET_ARTICLE_CATEGORY:
      return {
        ...state,
        articleCat: action.articleCat,
      };

    case VIEW_ARTICLE:
      return {
        ...state,
        viewArt: action.viewArt,
      };

    case DELETE_ARTICLE:
      return {
        ...state,
        article: state.article.filter((c) => c.articleId !== action.payLoad),
      };
    default:
      return state;
  }
};
