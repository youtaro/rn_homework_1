import {
  DELETE_USER,
  GET_ALL_USER,
  GET_USER,
} from "../../actions/Login/loginActions";

const initialState = {
  user: [],
  allUser: [],
};

export const loginReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        ...state,
        user: action.user,
      };
    case GET_ALL_USER:
      return {
        ...state,
        allUser: action.allUser,
      };
    case DELETE_USER:
      return {
        ...state,
        article: state.allUser.filter((u) => u.articleId !== action.payLoad),
      };

    default:
      return state;
  }
};
