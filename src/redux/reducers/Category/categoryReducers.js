import {
  DELETE_CATEGORY,
  GET_CATEGORY,
} from "../../actions/Category/categoryActions";

const initialState = {
  category: [],
};

export const categoryReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORY:
      return {
        ...state,
        category: action.category,
      };

    case DELETE_CATEGORY:
      return {
        ...state,
        category: state.category.filter((c) => c.categoryId !== action.payLoad),
      };

    default:
      return state;
  }
};
