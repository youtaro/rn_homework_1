import { Container } from "@material-ui/core";
import React, { Component } from "react";
import { Breadcrumb, Card, Image } from "react-bootstrap";
import { getUser } from "../../redux/actions/Login/loginActions";
import { viewArticle } from "../../redux/actions/Article/articleActions";
import { getCategory } from "../../redux/actions/Category/categoryActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Header from "../Home/Header";
import { Link } from "react-router-dom";

class ViewArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    let cc_id = localStorage.getItem("cccDD");
    this.props.viewArticle(this.props.match.params.id);
    this.props.getUser(cc_id);
    this.props.getCategory();
  }

  render() {
    return (
      <div>
        <Header />
        <Container className="mt-5">
          <Breadcrumb>
            <Breadcrumb.Item>
              <Link to="/home">អត្តបទ</Link>
            </Breadcrumb.Item>
            / អត្តបទលំអិត
          </Breadcrumb>
          <Card>
            <Card.Header>
              <div className="row">
                <div className="col-6">
                  <Image
                    src={this.props.viewArt?.user?.profile}
                    roundedCircle
                    height={38}
                    width={40}
                  />{" "}
                  <b> {this.props.viewArt?.user?.userName}</b>
                </div>
                <div className="col-6 text-right">
                  <div className="mt-2">
                    <span
                      className="p-1  text-center  text-white"
                      style={{
                        borderRadius: 5,
                        background: this.props.viewArt?.category?.color,
                      }}
                    >
                      <b> {this.props.viewArt?.category?.categoryName}</b>{" "}
                    </span>
                    |<b> {this.props.viewArt?.createdDate}</b>
                  </div>
                </div>
              </div>
            </Card.Header>
            <Card.Body>
              <p>{this.props.viewArt?.title}</p>
              <div className="text-center">
                <Image src={this.props.viewArt?.imageUrl} fluid />
              </div>
              <br />
              <p>{this.props.viewArt?.description}</p>
            </Card.Body>
          </Card>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  viewArt: state.articleReducers.viewArt,
  category: state.categoryReducers.category,
  user: state.loginReducers.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      viewArticle,
      getCategory,
      getUser,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewArticle);
