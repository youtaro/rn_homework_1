import { Button, Container, Snackbar } from "@material-ui/core";
import React, { Component } from "react";
import { Breadcrumb, Card, Form, Image } from "react-bootstrap";
import { getUser } from "../../redux/actions/Login/loginActions";
import {
  editArticle,
  updateArticle,
} from "../../redux/actions/Article/articleActions";
import { getCategory } from "../../redux/actions/Category/categoryActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Header from "../Home/Header";
import { Link } from "react-router-dom";
import ReactFilestack from "filestack-react";
import Alert from "@material-ui/lab/Alert";

class EditArticle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      img: "",
      description: "",
      category: "",
      open: false,
    };
  }

  componentDidMount() {
    let cc_id = localStorage.getItem("cccDD");
    editArticle(this.props.match.params.id, (datas) => {
      this.setState({
        title: datas.title,
        category: datas?.category?.categoryId,
        img: datas?.imageUrl,
        description: datas?.description,
      });
    });

    this.props.getUser(cc_id);
    this.props.getCategory();
  }

  handleUpdate = () => {
    let article = {
      category: {
        categoryId: this.state.category,
      },
      title: this.state.title,
      description: this.state.description,
      imageUrl: this.state.img,
      user: {
        userId: this.props.user.userId,
      },
    };
    updateArticle(this.props.match.params.id, article, (res) => {
      this.setState({
        open: true,
      });
      setTimeout(() => {
        this.props.history.push("/home");
      }, 2800);
    });
  };

  render() {
    const vertical = "top";
    const horizontal = "right";
    return (
      <div>
        <Header />
        <Container className="mt-5">
          <Snackbar
            anchorOrigin={{ vertical, horizontal }}
            open={this.state.open}
            key={vertical + horizontal}
            autoHideDuration={3000}
            onClose={() => this.setState({ open: false })}
          >
            <Alert onClose={this.handleClose} severity="success">
              "You Have Updated Article Successfully"
            </Alert>
          </Snackbar>
          <Breadcrumb>
            <Breadcrumb.Item>
              <Link to="/home">អត្តបទ</Link>
            </Breadcrumb.Item>
            / កែប្រែអត្តបទ
          </Breadcrumb>
          <Card body>
            <div className="row">
              <div className="col-11 offset-1">
                <div className="row">
                  <div className="col-7">
                    <Form>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>ចំណងជើងអត្តបទ</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="ចំណងជើងអត្តបទ"
                          value={this.state.title}
                          name="title"
                          onChange={(e) =>
                            this.setState({ title: e.target.value })
                          }
                        />
                      </Form.Group>

                      <Form.Group controlId="exampleForm.SelectCustom">
                        <Form.Label>ប្រភេទ</Form.Label>
                        <Form.Control
                          as="select"
                          custom
                          value={this.state.category}
                          onChange={(e) =>
                            this.setState({ category: e.target.value })
                          }
                        >
                          {this.props.category.map((datas, i) => (
                            <option key={i} value={datas.categoryId}>
                              {datas.categoryName}
                            </option>
                          ))}
                        </Form.Control>
                      </Form.Group>

                      <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Label>ការពិពណ៌នាអំពីអត្តបទ</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          placeholder="ការពិពណ៌នាអំពីអត្តបទ"
                          value={this.state.description}
                          onChange={(e) =>
                            this.setState({ description: e.target.value })
                          }
                        />
                      </Form.Group>
                    </Form>

                    <Button
                      variant="contained"
                      className="mb-2"
                      color="primary"
                      fullWidth="100%"
                      onClick={() => this.handleUpdate()}
                    >
                      កែប្រែអត្តបទ
                    </Button>
                  </div>
                  <div className="col-4">
                    <Image height="266px" width="355px" src={this.state.img} />
                    <div className="text-center">
                      <ReactFilestack
                        apikey={"AFuh3bZuaQCqcFDzeV30Tz"}
                        componentDisplayMode={{
                          type: "button",
                          customText: "Click Here To Upload",
                          customClass: " mt-2 text-center btn btn-warning",
                        }}
                        onSuccess={(res) =>
                          res.filesUploaded.map((data) =>
                            this.setState({ img: data.url })
                          )
                        }
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Card>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  viewArt: state.articleReducers.viewArt,
  category: state.categoryReducers.category,
  user: state.loginReducers.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      editArticle,
      getCategory,
      getUser,
      updateArticle,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(EditArticle);
