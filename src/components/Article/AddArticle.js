import React, { Component } from "react";
import ReactFilestack from "filestack-react";
import { Button, Card, Container, Snackbar } from "@material-ui/core";
import { Breadcrumb, Form, Image } from "react-bootstrap";
import Header from "../Home/Header";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getCategory } from "../../redux/actions/Category/categoryActions";
import { bindActionCreators } from "redux";
import { getUser } from "../../redux/actions/Login/loginActions";
import { addArticle } from "../../redux/actions/Article/articleActions";
import Alert from "@material-ui/lab/Alert";

class AddArticle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      title: "",
      img:
        "https://static1.squarespace.com/static/56fc1937c6fc08ffa7bc857a/591280c5579fb3effabdc016/59238e2f9f74568c561ae325/1503633039209/ThumbnailImage.png?format=1500w",
      description: "",
      category: "",
    };
  }

  componentDidMount() {
    let cc_id = localStorage.getItem("cccDD");
    this.props.getCategory();
    this.props.getUser(cc_id);
  }

  handleSubmit = () => {
    let article = {
      category: {
        categoryId: this.state.category,
      },
      title: this.state.title,
      description: this.state.description,
      imageUrl: this.state.img,
      user: {
        userId: this.props.user.userId,
      },
    };
    addArticle(article, (res) => {
      console.log(res);
      this.setState({
        open: true,
      });
      setTimeout(() => {
        this.props.history.push("/home");
      }, 2800);
    });
  };

  render() {
    const vertical = "top";
    const horizontal = "right";
    return (
      <div>
        <Header />
        <Container className="mt-5">
          <Snackbar
            anchorOrigin={{ vertical, horizontal }}
            open={this.state.open}
            key={vertical + horizontal}
            autoHideDuration={3000}
            onClose={() => this.setState({ open: false })}
          >
            <Alert onClose={this.handleClose} severity="success">
              "You Have Add Article Successfully"
            </Alert>
          </Snackbar>
          <Breadcrumb>
            <Breadcrumb.Item>
              <Link to="/home">អត្តបទ</Link>
            </Breadcrumb.Item>
            / បន្តែមអត្តបទ
          </Breadcrumb>
          <Card body>
            <div className="row">
              <div className="col-11 offset-1">
                <div className="row">
                  <div className="col-7">
                    <Form>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>ចំណងជើងអត្តបទ</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="ចំណងជើងអត្តបទ"
                          value={this.state.title}
                          name="title"
                          onChange={(e) =>
                            this.setState({ title: e.target.value })
                          }
                        />
                      </Form.Group>

                      <Form.Group controlId="exampleForm.SelectCustom">
                        <Form.Label>ប្រភេទ</Form.Label>
                        <Form.Control
                          as="select"
                          custom
                          value={this.state.category}
                          onChange={(e) =>
                            this.setState({ category: e.target.value })
                          }
                        >
                          {this.props.category.map((datas, i) => (
                            <option key={i} value={datas.categoryId}>
                              {datas.categoryName}
                            </option>
                          ))}
                        </Form.Control>
                      </Form.Group>

                      <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Label>ការពិពណ៌នាអំពីអត្តបទ</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          placeholder="ការពិពណ៌នាអំពីអត្តបទ"
                          value={this.state.description}
                          onChange={(e) =>
                            this.setState({ description: e.target.value })
                          }
                        />
                      </Form.Group>
                    </Form>

                    <Button
                      variant="contained"
                      className="mb-2"
                      color="primary"
                      fullWidth="100%"
                      onClick={() => this.handleSubmit()}
                    >
                      បញ្ចូលអត្តបទ
                    </Button>
                  </div>
                  <div className="col-4">
                    <Image height="266px" width="355px" src={this.state.img} />
                    <div className="text-center">
                      <ReactFilestack
                        componentDisplayMode={{
                          type: "button",
                          customText: "Click Here To Upload",
                          customClass: " mt-2 text-center btn btn-warning",
                        }}
                        apikey={"AFuh3bZuaQCqcFDzeV30Tz"}
                        onSuccess={(res) =>
                          res.filesUploaded.map((data) =>
                            this.setState({ img: data.url })
                          )
                        }
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Card>
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  viewArt: state.articleReducers.viewArt,
  category: state.categoryReducers.category,
  user: state.loginReducers.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getCategory,
      getUser,
      addArticle,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddArticle);
