import { Button, Container, Snackbar } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Col, ListGroup, Row, Tab, Card, Image, Modal } from "react-bootstrap";
import { getUser } from "../../redux/actions/Login/loginActions";
import {
  getArticle,
  deleteArticle,
} from "../../redux/actions/Article/articleActions";
import { getCategory } from "../../redux/actions/Category/categoryActions";
import { connect } from "react-redux";
import Alert from "@material-ui/lab/Alert";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";

const Articles = (props) => {
  useEffect(() => {
    let cc_id = localStorage.getItem("cccDD");
    props.getArticle();
    props.getUser(cc_id);
    props.getCategory();
  }, []);

  const [filter, setFilter] = useState("");
  const [modalShow, setModalShow] = useState(false);
  const [confirm, setConfirm] = useState(false);
  const [show, setShow] = useState(false);
  const [id, setId] = useState(0);

  const handleDelete = () => {
    if (confirm !== false) {
      props.deleteArticle(id);
      setModalShow(false);
      setShow(true);
    }
  };

  const deleteConfirm = (id) => {
    setModalShow(true);
    setId(id);
    setConfirm(true);
  };

  const vertical = "top";
  const horizontal = "right";
  return (
    <div>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={show}
        key={vertical + horizontal}
        autoHideDuration={3000}
        onClose={() => setShow(false)}
      >
        <Alert severity="success">You Have Deleted Article Succesfully</Alert>
      </Snackbar>
      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">បញ្ជាក់</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>លុបរួចមិនអាចកែប្រែបានទេ</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="contained"
            color="primary"
            onClick={() => setModalShow(false)}
          >
            ថយក្រោយ
          </Button>

          <Button
            className="mx-1"
            variant="contained"
            color="secondary"
            onClick={() => handleDelete()}
          >
            យល់ព្រម
          </Button>
        </Modal.Footer>
      </Modal>

      <Container className="mt-5">
        <Link to="/add_article">
          <Button variant="contained" className="mb-2" color="primary">
            បន្តែមអត្តបទថ្មី
          </Button>
        </Link>

        <Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">
          <Row>
            <Col sm={3}>
              <ListGroup>
                <ListGroup.Item
                  href="#link1"
                  // className="active"
                  action
                  onClick={() => setFilter("")}
                >
                  អត្តបទគ្រប់ប្រភេទ
                </ListGroup.Item>
                {props.category.map((datas, i) => (
                  <ListGroup.Item
                    action
                    onClick={() => setFilter(datas.categoryName)}
                    href={`#${datas.categoryId}`}
                    key={i}
                  >
                    {datas.categoryName}
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </Col>
            <Col sm={8} className="offset-1">
              {props.article.map((datas, i) => {
                if (filter.length !== 0) {
                  const newPost = datas.category.categoryName.toLowerCase();
                  if (newPost.startsWith(filter.toLowerCase())) {
                    return (
                      <Card className="mb-2" key={i}>
                        <Card.Header>
                          <div className="row">
                            <div className="col-6">
                              <Image
                                src={datas?.user?.profile}
                                roundedCircle
                                height={38}
                                width={40}
                              />{" "}
                              <b> {datas?.user?.userName}</b>
                            </div>
                            <div className="col-6 text-right">
                              <div className="mt-2">
                                <span
                                  className="p-1   text-white"
                                  style={{
                                    borderRadius: 5,
                                    background: datas?.category?.color,
                                  }}
                                >
                                  <b> {datas?.category?.categoryName}</b>{" "}
                                </span>
                                |<b> {datas?.createdDate}</b>
                              </div>
                            </div>
                          </div>
                        </Card.Header>
                        <Card.Body>
                          <p>{datas.title}</p>
                          <div className="text-center">
                            <Image src={datas.imageUrl} fluid />
                          </div>
                          <br />
                          <div className="mt-2">
                            <Button
                              variant="contained"
                              color="primary"
                              className="float-right "
                              style={{ background: datas?.category?.color }}
                            >
                              <Link
                                to={`/view_article/${datas.articleId}`}
                                className=" text-decoration-none text-white"
                              >
                                See More...
                              </Link>
                            </Button>
                            {props.user.userName !== datas?.user?.userName ? (
                              ""
                            ) : (
                              <>
                                <Button
                                  className="mx-1 "
                                  variant="contained"
                                  color="primary"
                                >
                                  <Link
                                    to={`/edit_article/${datas.articleId}`}
                                    className=" text-decoration-none text-white"
                                  >
                                    <i className="fas fa-edit"></i>{" "}
                                  </Link>
                                </Button>

                                <Button
                                  className="mx-1 "
                                  variant="contained"
                                  color="secondary"
                                  onClick={() => {
                                    deleteConfirm(datas.articleId);
                                  }}
                                >
                                  <Link className=" text-decoration-none text-white">
                                    <i className="fas fa-trash-alt    "></i>
                                  </Link>
                                </Button>
                              </>
                            )}
                          </div>
                        </Card.Body>
                      </Card>
                    );
                  } else {
                    return null;
                  }
                }
                return (
                  <Card className="mb-2" key={i}>
                    <Card.Header>
                      <div className="row">
                        <div className="col-6">
                          <Image
                            src={datas?.user?.profile}
                            roundedCircle
                            height={38}
                            width={40}
                          />{" "}
                          <b> {datas?.user?.userName}</b>
                        </div>
                        <div className="col-6 text-right">
                          <div className="mt-2">
                            <span
                              className="p-1   text-white"
                              style={{
                                borderRadius: 5,
                                background: datas?.category?.color,
                              }}
                            >
                              <b> {datas?.category?.categoryName}</b>{" "}
                            </span>
                            |<b> {datas?.createdDate}</b>
                          </div>
                        </div>
                      </div>
                    </Card.Header>
                    <Card.Body>
                      <p>{datas.title}</p>
                      <div className="text-center">
                        <Image src={datas.imageUrl} fluid />
                      </div>
                      <br />
                      <div className="mt-2">
                        <Button
                          variant="contained"
                          color="primary"
                          className="float-right "
                          style={{ background: datas?.category?.color }}
                        >
                          <Link
                            to={`/view_article/${datas.articleId}`}
                            className=" text-decoration-none text-white"
                          >
                            See More...
                          </Link>
                        </Button>
                        {props.user.userName !== datas?.user?.userName ? (
                          ""
                        ) : (
                          <>
                            <Button
                              className="mx-1 "
                              variant="contained"
                              color="primary"
                            >
                              <Link
                                to={`/edit_article/${datas.articleId}`}
                                className=" text-decoration-none text-white"
                              >
                                <i className="fas fa-edit"></i>{" "}
                              </Link>
                            </Button>

                            <Button
                              className="mx-1 "
                              variant="contained"
                              color="secondary"
                              onClick={() => {
                                deleteConfirm(datas.articleId);
                              }}
                            >
                              <Link className=" text-decoration-none text-white">
                                <i className="fas fa-trash-alt    "></i>
                              </Link>
                            </Button>
                          </>
                        )}
                      </div>
                    </Card.Body>
                  </Card>
                );
              })}
            </Col>
          </Row>
        </Tab.Container>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  article: state.articleReducers.article,
  category: state.categoryReducers.category,
  user: state.loginReducers.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getArticle,
      getCategory,
      getUser,
      deleteArticle,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Articles);
