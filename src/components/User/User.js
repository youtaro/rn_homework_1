import { Button, Container, Snackbar } from "@material-ui/core";
import React, { Component } from "react";
import { Form, Image, Modal, Table } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getAllUser,
  updateUser,
  deleteUser,
  getUser,
} from "../../redux/actions/Login/loginActions";
import Header from "../Home/Header";
import Alert from "@material-ui/lab/Alert";

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      show: false,
      confirm: false,
      modalShow: false,
      email: "",
      img: "",
      name: "",
      id: 0,
      msg: "You Have Updated Successfully",
    };
  }
  componentDidMount() {
    this.props.getAllUser();
    let cc_id = localStorage.getItem("cccDD");
    this.props.getUser(cc_id);
  }

  handleClose = () => {
    this.setState({
      show: false,
      modalShow: false,
    });
  };

  handleUserUpdate = () => {
    let user = {
      email: this.state.email,
      profile: this.state.img,
      userName: this.state.name,
    };
    updateUser(this.state.id, user, (res) => {
      this.setState({
        open: true,
        show: false,
      });
      this.props.getAllUser();
    });
  };

  handleDelete = () => {
    if (this.state.confirm !== false) {
      this.props.deleteUser(this.state.id);
      this.setState({
        modalShow: false,
        show: true,
        msg: "You have deleted Successfully",
      });
    }
  };

  deleteConfirm = (id) => {
    this.setState({
      modalShow: true,
      id: id,
      confirm: true,
    });
  };

  handleEdit = (email, profile, name, id) => {
    console.log(this.state);
    this.setState({
      show: true,
      email: email,
      img: profile,
      name: name,
      id: id,
    });
  };

  render() {
    const vertical = "top";
    const horizontal = "right";
    return (
      <div>
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={this.state.open}
          key={vertical + horizontal}
          autoHideDuration={3000}
          onClose={() => this.setState({ open: false })}
        >
          <Alert onClose={this.handleClose} severity="success">
            {this.state.msg}
          </Alert>
        </Snackbar>
        <Modal show={this.state.show} size="lg" onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>កែប្រែប្រើប្រាស់</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>ឈ្មោះ</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="ឈ្មោះ"
                  value={this.state.name}
                  name="name"
                  onChange={(e) => this.setState({ name: e.target.value })}
                />
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>រូប</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="រូប"
                  value={this.state.img}
                  name="img"
                  onChange={(e) => this.setState({ img: e.target.value })}
                />
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>អ៊ីមែល</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="អ៊ីមែល"
                  value={this.state.email}
                  name="email"
                  onChange={(e) => this.setState({ email: e.target.value })}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="contained"
              className="mb-2"
              color="primary"
              fullWidth="100%"
              onClick={() => this.handleUserUpdate()}
            >
              កែប្រែអ្នកប្រើ
            </Button>
          </Modal.Footer>
        </Modal>
        <Header />
        <Container className="mt-3">
          <h2>អ្នកប្រើប្រាស់</h2>

          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>UserName</th>
                <th>Email</th>
                <th>profile</th>
                <th className="text-center" width="10%">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.allUser.map((datas, i) => (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{datas.userName}</td>
                  <td>{datas.email}</td>
                  <td>
                    <Image
                      src={datas.profile}
                      roundedCircle
                      height={38}
                      width={40}
                    />{" "}
                  </td>
                  <td className="text-center">
                    <div
                      className="btn btn-sm btn-warning"
                      onClick={() =>
                        this.handleEdit(
                          datas.email,
                          datas.profile,
                          datas.userName,
                          datas.userId
                        )
                      }
                    >
                      <i class="fas fa-edit    "></i>
                    </div>
                    <div
                      className="btn btn-sm mx-1 btn-danger"
                      onClick={() => this.deleteConfirm(datas.userId)}
                    >
                      <i class="fas fa-trash-alt    "></i>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Modal
            show={this.state.modalShow}
            onHide={() => this.setState({ modalShow: false })}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                បញ្ជាក់
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <h4>លុបរួចមិនអាចកែប្រែបានទេ</h4>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="contained"
                color="primary"
                onClick={() => this.setState({ modalShow: false })}
              >
                ថយក្រោយ
              </Button>

              <Button
                className="mx-1"
                variant="contained"
                color="secondary"
                onClick={() => this.handleDelete()}
              >
                យល់ព្រម
              </Button>
            </Modal.Footer>
          </Modal>
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  allUser: state.loginReducers.allUser,
  user: state.loginReducers.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getAllUser,
      updateUser,
      deleteUser,
      getUser,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
