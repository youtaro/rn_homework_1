import { Button, Container, Snackbar } from "@material-ui/core";
import React, { Component } from "react";
import { Form, Image, Modal, Table } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getCategory,
  addACategory,
  updateCategory,
  deleteCategory,
} from "../../redux/actions/Category/categoryActions";
import { getUser } from "../../redux/actions/Login/loginActions";
import Header from "../Home/Header";
import Alert from "@material-ui/lab/Alert";

class Category extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      color: "",
      name: "",
      open: "",
      id: 0,
      isUpdate: false,
      msg: "You Have Add Article Successfully",
      confirm: false,
      modalShow: false,
    };
  }
  componentDidMount() {
    let cc_id = localStorage.getItem("cccDD");
    this.props.getCategory();
    this.props.getUser(cc_id);
  }

  handleClose = () => {
    this.setState({
      show: false,
    });
  };

  handleSubmit = () => {
    let category = {
      categoryName: this.state.name,
      color: this.state.color,
    };
    addACategory(category, (res) => {
      this.setState({
        open: true,
        show: false,
      });
      this.props.getCategory();
    });
  };

  addModal = () => {
    this.setState({
      show: true,
      isUpdate: false,
      name: "",
      color: "",
    });
  };

  handleUpdate = () => {
    let category = {
      categoryName: this.state.name,
      color: this.state.color,
    };
    updateCategory(this.state.id, category, (res) => {
      this.setState({
        msg: "You Have Updated Succesfully",
        open: true,
        show: false,
      });
      this.props.getCategory();
    });
  };

  handleDelete = () => {
    if (this.state.confirm !== false) {
      this.props.deleteCategory(this.state.id);
      this.setState({
        modalShow: false,
        open: true,
        msg: "You have deleted Successfully",
      });
    }
  };

  deleteConfirm = (id) => {
    this.setState({
      modalShow: true,
      id: id,
      confirm: true,
    });
  };

  handleEdit = (color, name, id) => {
    console.log(this.state);
    this.setState({
      show: true,
      name: name,
      color: color,
      isUpdate: true,
      id: id,
    });
  };

  render() {
    const vertical = "top";
    const horizontal = "right";
    return (
      <div>
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={this.state.open}
          key={vertical + horizontal}
          autoHideDuration={3000}
          onClose={() => this.setState({ open: false })}
        >
          <Alert onClose={this.handleClose} severity="success">
            {this.state.msg}
          </Alert>
        </Snackbar>
        <Modal show={this.state.show} size="lg" onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>
              {" "}
              {this.state.isUpdate ? "កែប្រែប្រភេទ" : "បន្ថែមប្រភេទ"}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>ឈ្មោះប្រភេទ</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="ឈ្មោះ"
                  value={this.state.name}
                  name="name"
                  onChange={(e) => this.setState({ name: e.target.value })}
                />
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>ពណ៌</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="ពណ៌"
                  value={this.state.color}
                  name="color"
                  onChange={(e) => this.setState({ color: e.target.value })}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="contained"
              className="mb-2"
              color="primary"
              fullWidth="100%"
              onClick={() => {
                this.state.isUpdate ? this.handleUpdate() : this.handleSubmit();
              }}
            >
              {this.state.isUpdate ? "កែប្រែប្រភេទ" : "បន្ថែមប្រភេទ"}
            </Button>
          </Modal.Footer>
        </Modal>
        <Header />
        <Container className="mt-3">
          <Button
            variant="contained"
            className="mb-2"
            color="primary"
            onClick={() => this.addModal()}
          >
            បន្តែមប្រភេទអត្តបទថ្មី
          </Button>

          <h2>ប្រភេទអត្តបទ</h2>

          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Category Name</th>
                <th>Color</th>
                <th className="text-center" width="10%">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.category.map((datas, i) => (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{datas.categoryName}</td>
                  <td>
                    <Image
                      style={{ background: datas.color }}
                      roundedCircle
                      height={38}
                      width={40}
                    />{" "}
                  </td>
                  <td className="text-center">
                    <div
                      className="btn btn-sm btn-warning"
                      onClick={() =>
                        this.handleEdit(
                          datas.color,
                          datas.categoryName,
                          datas.categoryId
                        )
                      }
                    >
                      <i class="fas fa-edit    "></i>
                    </div>
                    <div
                      className="btn btn-sm mx-1 btn-danger"
                      onClick={() => this.deleteConfirm(datas.categoryId)}
                    >
                      <i class="fas fa-trash-alt    "></i>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Modal
            show={this.state.modalShow}
            onHide={() => this.setState({ modalShow: false })}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                បញ្ជាក់
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <h4>លុបរួចមិនអាចកែប្រែបានទេ</h4>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="contained"
                color="primary"
                onClick={() => this.setState({ modalShow: false })}
              >
                ថយក្រោយ
              </Button>

              <Button
                className="mx-1"
                variant="contained"
                color="secondary"
                onClick={() => this.handleDelete()}
              >
                យល់ព្រម
              </Button>
            </Modal.Footer>
          </Modal>
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  category: state.categoryReducers.category,
  user: state.loginReducers.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getCategory,
      getUser,
      addACategory,
      deleteCategory,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Category);
