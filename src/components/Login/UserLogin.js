import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Snackbar, TextField } from "@material-ui/core";
import "./../../App.css";
import { getUser } from "../../redux/actions/Login/loginActions";
import { bindActionCreators } from "redux";
import { Form, Spinner } from "react-bootstrap";
import Alert from "@material-ui/lab/Alert";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";

export class UserLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eamil: "",
      loading: false,
      isAuth: false,
      alert: {
        open: false,
        vertical: "top",
        horizontal: "right",
      },
      warnMsg: "",
    };
  }

  login = (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    this.props.getUser(this.state.email).then(() => {
      if (this.props.user.email !== undefined) {
        this.setState({
          isAuth: true,
          loading: false,
        });
      }

      if (this.state.email === "" || this.props.user.email === undefined) {
        this.setState({
          loading: false,
          alert: { open: true },
          warnMsg: " Please input your email",
        });
      } else {
        this.setState({
          email: "",
          loading: false,
          alert: { open: true },
          warnMsg: "     Please check your email and try again!!!",
        });
      }
    });
  };

  render() {
    const vertical = "top";
    const horizontal = "right";
    localStorage.setItem("cccDD", this.state.email);
    if (!this.state.isAuth) {
      return (
        <div>
          <>
            <Snackbar
              anchorOrigin={{ vertical, horizontal }}
              open={this.state.alert.open}
              key={vertical + horizontal}
              autoHideDuration={3000}
              onClose={() => this.setState({ alert: { open: false } })}
            >
              <Alert
                onClose={this.handleClose}
                severity={this.state.isAuth ? "success" : "error"}
              >
                {this.state.warnMsg}
              </Alert>
            </Snackbar>
            <div className="gg">
              <Form>
                <div>
                  <h1
                    className=" mb-3 text-center"
                    style={{ color: "#3f51b5" }}
                  >
                    Welcome to React Course{" "}
                    {/* <span aria-label="" role="img">
                    🤟
                  </span> */}
                  </h1>
                </div>
                <div className="box p-5">
                  <TextField
                    id="outlined-full-width"
                    label="Email"
                    placeholder="Pleease input ur email"
                    fullWidth
                    autoComplete="off"
                    margin="normal"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                    name="email"
                    value={this.state.email}
                    onChange={(e) => this.setState({ email: e.target.value })}
                  />
                  <Button
                    type="submit"
                    className=" mt-2 "
                    style={{ width: "100%" }}
                    variant="contained"
                    color="primary"
                    size="large"
                    onClick={(e) => this.login(e)}
                  >
                    {this.state.loading ? (
                      <Spinner animation="border" />
                    ) : (
                      "Login"
                    )}
                  </Button>{" "}
                  <div className="text-center">
                    Not a member? {""}
                    <Link to="/register">SingUp Now!!</Link>
                  </div>
                </div>

                <h5 className=" text-center mt-2" style={{ color: "#3f51b5" }}>
                  © 2020 React course, All rights reserved.
                </h5>
              </Form>
            </div>
          </>
        </div>
      );
    } else {
      return <Redirect to="/home" />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.loginReducers.user,
    checkAuth: state.loginReducers.checkAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getUser,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(UserLogin);
