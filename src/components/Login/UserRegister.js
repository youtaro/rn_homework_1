import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Snackbar, TextField } from "@material-ui/core";
import "./../../App.css";
import { addUser } from "../../redux/actions/Login/loginActions";
import { bindActionCreators } from "redux";
import { Form, Spinner } from "react-bootstrap";
import Alert from "@material-ui/lab/Alert";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";

export class UserRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      profile: "",
      loading: false,
      isAuth: false,
      open: false,
      warnMsg: "",
    };
  }

  handleRegister = () => {
    this.setState({
      loading: true,
    });
    let user = {
      email: this.state.email,
      profile: this.state.profile,
      userName: this.state.name,
    };

    addUser(user, (res) => {
      console.log(res);
      this.setState({});
      this.setState({
        open: true,
      });
      setTimeout(() => {
        this.props.history.push("/");
      }, 2800);
    });
  };

  render() {
    const vertical = "top";
    const horizontal = "right";
    localStorage.setItem("cccDD", this.state.email);
    return (
      <div>
        <>
          <Snackbar
            anchorOrigin={{ vertical, horizontal }}
            open={this.state.open}
            key={vertical + horizontal}
            autoHideDuration={3000}
            onClose={() => this.setState({ open: false })}
          >
            <Alert onClose={this.handleClose} severity="success">
              "You Have Register Successfully"
            </Alert>
          </Snackbar>
          <div className="gg">
            <Form>
              <div>
                <h1 className=" mb-3 text-center" style={{ color: "#3f51b5" }}>
                  User Register{" "}
                  {/* <span aria-label="" role="img">
                    🤟
                  </span> */}
                </h1>
              </div>
              <div className="box p-5">
                <TextField
                  id="outlined-full-width"
                  label="Email"
                  placeholder="Pleease input ur email"
                  fullWidth
                  autoComplete="off"
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                  name="email"
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                />
                {/* === */}
                <TextField
                  id="outlined-full-width"
                  label="User Profile"
                  placeholder="Pleease input ur profile"
                  fullWidth
                  autoComplete="off"
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                  name="profile"
                  value={this.state.profile}
                  onChange={(e) => this.setState({ profile: e.target.value })}
                />
                {/* === */}
                <TextField
                  id="outlined-full-width"
                  label="User Name"
                  placeholder="Pleease input ur name"
                  fullWidth
                  autoComplete="off"
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                  name="name"
                  value={this.state.name}
                  onChange={(e) => this.setState({ name: e.target.value })}
                />
                <Button
                  className=" mt-2 "
                  style={{ width: "100%" }}
                  variant="contained"
                  color="primary"
                  size="large"
                  onClick={() => this.handleRegister()}
                >
                  {this.state.loading ? (
                    <Spinner animation="border" />
                  ) : (
                    "Register"
                  )}
                </Button>{" "}
              </div>

              <h5 className=" text-center mt-2" style={{ color: "#3f51b5" }}>
                © 2020 React course, All rights reserved.
              </h5>
            </Form>
          </div>
        </>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.loginReducers.user,
    checkAuth: state.loginReducers.checkAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      addUser,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(UserRegister);
