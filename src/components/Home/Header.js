import React, { useEffect } from "react";
import { Nav, Navbar, NavDropdown, Image } from "react-bootstrap";
import { Container } from "@material-ui/core";
import { connect } from "react-redux";
import { Link, NavLink } from "react-router-dom";
import { bindActionCreators } from "redux";
import { getUser } from "../../redux/actions/Login/loginActions";

const Header = (props) => {
  useEffect(() => {
    let cc_id = localStorage.getItem("cccDD");
    props.getUser(cc_id);
  }, []);
  return (
    <div className="bg-light">
      <Container>
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
          <Navbar.Brand href="/">អត្តបទពត៌មាន</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link as={NavLink} to="/home">
                អត្តបទ
              </Nav.Link>{" "}
              <Nav.Link as={NavLink} to="/user">
                អ្នកប្រើប្រាស់
              </Nav.Link>
              <Nav.Link as={NavLink} to="/category">
                ប្រភេទអត្តបទ
              </Nav.Link>
              <Image
                src={props.user.profile}
                roundedCircle
                height={38}
                width={40}
              />
              <NavDropdown
                title={props.user.userName}
                id="collasible-nav-dropdown"
              >
                {/* <NavDropdown.Item> */}{" "}
                <Link className="text-success ml-2 text-decoration-none" to="/">
                  Logout
                </Link>
                {/* </NavDropdown.Item> */}
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.loginReducers.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getUser,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
