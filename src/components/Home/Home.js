import React from "react";

import Header from "./Header";

import Articles from "../Article/Articles";

const Home = (props) => {
  return (
    <div>
      <Header />
      <Articles />
    </div>
  );
};

export default Home;
